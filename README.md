# gists

## Small pieces of code for various purposes

### rmarkdown/spin

- [rmarkdown/spin](https://gitlab.com/jozefhajnala/gists/tree/master/rmarkdown/spin) has working examples of turning R scripts into professional html reports, with cusom css
- it serves as accompanying code for the [How to create professional reports from R scripts, with custom styles](https://jozefhajnala.gitlab.io/r/r913-spin-with-style/) blogpost

### rstudio/snippets

- [rstudio/snippets](https://gitlab.com/jozefhajnala/gists/tree/master/rstudio/snippets) has RStudio Code Snippets - text macros that are used for quickly inserting (and potentially executing) common chunks of code

### oldestR

- [oldestR](https://gitlab.com/jozefhajnala/gists/tree/master/oldestR) has some of my oldest R code I was able to find, unedited 
- dates somewhere to the beginning of 2008
- it serves as accompanying example for the blogpost [Why 2019 is a great year to start with R: A story of 10 year old R code](https://jozefhajnala.gitlab.io/r/r900-10-year-old-code/)
- and for the LOLs of course ;-)