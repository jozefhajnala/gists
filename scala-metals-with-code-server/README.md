# Using the Scala Metals extension with VS Code Server

## Introduction

- [Code server](https://github.com/cdr/code-server) is VS Code running on a remote server, accessible through the browser. 
- [Scala (Metals)](https://marketplace.visualstudio.com/items?itemName=scalameta.metals#overview) is an extension for VS Code with many useful features such as Import build, Completions, Goto definition and more

Unfortunately, the extension was not yet ported to Code Server. This document tries to show how to install the extension on VS Code server manually and enjoy better Scala development on remote servers.

## Step-by-step guide

1. Download metals from the [Scala (Metals) page on Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=scalameta.metals):  - Download Extension link on the right-hand side

2. Open the downloaded .vsix file with a zip tool

3. Open `extension/package.json` for editing in a text editor

4. In the `package.json`, locate `"dependencies": {` and change `"vscode"` to your VS Code Server version. For example, if that version is `1.33.1`:
  
```
"vscode": "^1.33.1"
```

5. In the `package.json`, locate `"engines": {` and change `"vscode"` to your VS Code Server version, for example, if that version is `1.33.1`:
  ```
"vscode": "^1.33.1"
```

6. Replace the `package.json` in the .vsix file with the updated version

7. In VS Code Server, go to Extensions -> More Actions... -> Install from VSIX... and select the updated .vsix in the dialog